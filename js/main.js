Vue.component('fruits', {
    props: ['fruit'],
    mounted() {
        if (this.fruit) {
            console.log(this.fruit);
        }
    }
});

Vue.component('master', {
    template: `
        <div>
            <h1>Master component</h1>
            <child></child>
        </div>
    `,
});

Vue.component('child', {
    template: `
        <div>
            <p style="background: yellow;">Soy un parrafo hijo</p>
        </div>
    `,
});

Vue.component('movies', {
    template: `
        <h1>Component {{title}}</h1>
    `,
    data() {
        return {
            title: 'MOVIES',
        };
    }
});

Vue.component('posts', {
    template: `
        <div class="component-posts">
            <ul v-if="posts">
                <li v-for="post in posts">{{post.title}}</li>
            </ul>
            <span v-else>Cargando lista de posts</span>
        </div>
    `,
    mounted() {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then((result) => {
                this.posts = result.data;
            });
    },
    data() {
        return {
            posts: null
        }
    }
})

Vue.filter('uppercase', (value) => value.toUpperCase());

new Vue({
    el: 'main',
    mounted() {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then((result) => {
                /* console.log(result.data); */
                this.posts = result.data;
            });
    },
    data: {
        texto: 'Hola mundo desde vue.js 2',
        default_name: 'Default name',
        qualification: 5,
        series: ['Bojack Horseman', 'Game of Thrones', 'Breaking bad', 'Mr. Robot'],
        fruits: [{
                name: 'Apple',
                season: 'winter',
                price: 'low'
            },
            {
                name: 'Orange',
                season: 'autumn',
                price: 'mid'
            },
            {
                name: 'Raspberry',
                season: 'spring',
                price: 'high'
            },
            {
                name: 'Watermelon',
                season: 'summer',
                price: 'mid'
            }
        ],
        superfruit: {
            name: 'grape',
            season: 'winter',
            price: 'low'
        },
        newMovie: null,
        name: 'BoJack',
        surname: 'Horseman',
        searchParameter: '',
        confirmed: null,
        posts: null,
        choosen: null,
    },
    methods: {
        addMovie() {
            /* alert(this.newMovie); */
            this.series.unshift(this.newMovie);
            this.newMovie = null;
        },
        deleteMovie(index) {
            /* alert('Delete Film '+index); */
            this.series.splice(index, 1);
        },
        checkFruit(id) {
            this.confirmed = id;
        }
    },
    computed: {
        nameAndSurname() {
            return this.name + " " + this.surname;
        },
        orderedMovies() {
            return this.series.sort();
        },
        searchMovie() {
            return this.series.filter((movie) =>
                movie.includes(this.searchParameter)
            );
        },
        searchFruit() {
            return this.fruits.filter((fruit) =>
                fruit.name.includes(this.searchParameter)
            );
        }
    }
});

const vue2 = new Vue({
    el: '#app',
    data: {
        text: 'Second instance of Vue'
    }
});

const vue3 = new Vue({
    el: '#vue3',
    data: {
        text: 'Third instance of Vue'
    }
});