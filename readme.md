# Apuntes

## Instalar nodejs

[https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions](https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions)

```bash
curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -
sudo apt-get install -y nodejs
```

Actualizar el gestor de paquetes `npm`

```bash
sudo npm install -g npm
```

## Crear proyecto

### 02x03_crear_proyecto

### 02x05_instalar_hola_mundo

Vamos a usar vue.js desde el **CDN** de vue.js
[https://vuejs.org/v2/guide/installation.html#CDN](https://vuejs.org/v2/guide/installation.html#CDN)

Después de crear una carpeta `js` que contendrá los javascripts, creamos dentro un fichero `main.js`. Dentro se creará una instancia de **vue** y que recibirá por parámetros un _json_ con los elementos que usaremos luego en `index.html`

```javascript
new Vue({
    el: 'principal',
    data : {
        texto: 'Hola mundo desde vue.js 2'
    }
});
```

En el `index.html` se puede acceder a estos valores a través de la nueva etiqueta creada `<principal>` y su contenido con `{{ }}`

```html
<principal>
    <h1>{{texto}}</h1>
</principal>
```

Pero también se puede utilizar la _propiedad_ o en **vue** llamado **_directiva_** `v-text` para imprimir valores

```html
<h2 v-text="texto"/>
```

Pero el elemento `principal` definido en la instancia de **vue** no es un elemento válido de **vue**. Para evitar este error debemos cambiar `principal` por `main` en el _js_ y en el _html_.

>main.js

```js
new Vue({
    el: 'main',
    data : {
        texto: 'Hola mundo desde vue.js 2'
    }
});
```

>index.html

```html
<body>
    <main>
        <h1>{{texto}}</h1>
        <h2 v-text="texto"/>
    </main>

    <script src="https://unpkg.com/vue@2.4.4/dist/vue.js"></script>
    <script src="js/main.js"></script>
</body>
```

# Las bases de VueJS 2

## Two way data-binding y reactividad

### 03x01_Two_way_data-binding_y_reactividad

Se puede actualizar el valor de una variable en tiempo real _data-binding_ usando la directiva `v-model` para poder trabajar directamente sobre su valor. Para eso añadimos un nuevo valor al _data_ del **main.js**
>main.js

```js
default_name: 'Default name'
```

Y podremos acceder al valor de este model de esta manera en tiempo real:

>main.html

```html
<input type="text" v-model="default_name"/>
<p>El nombre es: {{default_name}}</p>
```

Si queremos mostrar todo el valor de _data_ que hemos definido en **main.js** podemos acceder a través de:
>main.html

```html
<pre>{{$data}}</pre>
```

## Directivas condicionales

### 03x02_directivas_condicionales

Para utilizar condiciones _if_ dentro de la vista con **vue** se puede utilizar las directivas `v-if`. Por ejemplo vamos a crear un nuevo valor dentro del modelo llamado `qualification` y luego accedemos a través de el con un _data_binding_ y mostraremos un resultado u otro dependiendo de su valor.

>main.js

```js
qualification: 5
```

>index.html

```html
<input type="text" v-model="qualification"/>
<strong v-if="qualification && qualification >= 5">
    ¡¡Aprobado!! tu nota es {{qualification}}
</strong>
<span v-else-if="qualification">
    ¡¡Suspendiste!! tu nota es {{qualification}}
</span>
```

## Directivas de iteración

### 03x03_directivas_de_iteracion

Para recorrer un listado de un objeto podemos usar la directiva `v-for` con la notación `objeto in listaDeObjetos` de esta manera se crea un nuevo `objeto` por cada elemento de `listaDeObjetos` y después acceder a él a través de `{{objeto}}` como por ejemplo:

>main.js

```js
series: ['Bojack Horseman','Game of Thrones','Breaking bad','Mr. Robot']
```

>index.html
```html
<h1>Listado de series</h1>
<ol>
    <li v-for="serie in series">{{serie}}</li>
</ol>
```

## Mostrar listado (colección de objetos)
### 03x04_lista_de_objetos

Si tuvieramos una lista de objetos, también podremos recorrerla de la siguiente forma:

>main.js

```js
fruits: [
            {
                name: 'Apple',
                season: 'winter',
                price: 'low'
            },
            {
                name: 'Orange',
                season: 'autumn',
                price: 'mid'
            },
            {
                name: 'Raspberry',
                season: 'spring',
                price: 'high'
            },
            {
                name: 'Watermelon',
                season: 'summer',
                price: 'mid'
            }
        ]
```

>index.html

```html
<h1>Lista de frutas</h1>
<ul>
    <li v-for="(fruit, index) in fruits">
        {{index+1}}: {{fruit.name}} - {{fruit.season}}
    </li>
</ul>
```

### 03x05_recorrer_objeto

También podemos recorrer las propiedades de un objeto con `v-for` de la siguiente forma

>main.js

```js
superfruit: {
    name: 'grape',
    season: 'winter',
    price: 'low'
}
```

>index.html

```html
<h1>Recorrer elementos superfruta</h1>
<ul>
    <li v-for="(value, key, index) in superfruit">
        {{index}}: {{key}} - {{value}}
    </li>
</ul>
```

## Eventos (Métodos)

### 03x06_eventos

Para poder crear métodos con **vue.js** puedes añadir dentro de la instancia de `Vue` los métodos que quieras. Tienes que añadirlos dentro del objeto `methods:`

>main.js

```js
new Vue({
            el: 'main',
            data: {
                newMovie: null
            },
            methods: {
                addMovie() {
                    alert(this.newMovie);
                },
                deleteMovie() {
                    alert('Delete Film');
                }
            }
```

La manera de llamar a estos métodos desde la vista _html_ es usando las directivas `<button v-on:click="addMovie()">` o `<button @click="deleteMovie()">`. 

Para el ejemplo vamos a usar un formulario, y para ello vamos a utilizar el evento `@submit="addMovie()"` **PERO** vamos a añadir `.prevent` para que el formulario no se envie (Que es lo que hace por defecto HTML pero nosotros queremos que no se envíe a ningún lugar pues vamos a llamar a un método que tenemos en el _javascript_ ya cargado).

Además vamos a añadir un listado que se actualizará en tiempo real, y que podremos añadir o eliminar elementos.

>main.js

```js
new Vue({
    el: 'main',
    data: {
        series: ['Bojack Horseman', 'Game of Thrones', 'Breaking bad', 'Mr. Robot'],
        newMovie: null
    },
    methods: {
        addMovie() {
            this.series.unshift(this.newMovie);
            this.newMovie = null;
        },
        deleteMovie(index) {
            this.series.splice(index, 1);
        }
    }
});
```

>index.html

```html
<form @submit.prevent="addMovie()">
    <input type="text" v-model="newMovie">
    <input type="submit" value="Añadir película">
</form>

<h1>Listado de series</h1>
<ol>
    <li v-for="(serie,index) in series">
        {{serie}}
        <button @click="deleteMovie(index)">Eliminar serie</button>
    </li>
</ol>
```

## Propiedades computadas

### 03x07_propiedades_computadas

Hay momentos en los que queremos mostrar cierta información ordenada o con un formato específico. Esto requiere cierta grado de lógica de computación. Si queremos seguir un patrón separado para la _Vista_ y otro para la lógica, **vue.js** nos proporciona los métodos `computed:{}` que nos ayudarán a mantener esa separación entre la _Vista_ y el la lógica de la app.

Un ejemplo sencillo sería unir dos cadenas de texto en una. Para ello creamos una nueva propiedad computada en el bloque `computed` y desde la _Vista - html_ podemos hacer referncia a esta propiedad **como un objeto más**, por eso no añadiremos los paréntesis `()`

>main.js

```js
    data: {
        name: 'BoJack',
        surname: 'Horseman',
        series: ['Bojack Horseman', 'Game of Thrones', 'Breaking bad', 'Mr. Robot']
    },
    computed: {
        nameAndSurname() {
            return this.name + " " + this.surname;
        },
        orderedMovies(){
            return this.series.sort();
        }
    }
```

>index.html

```html
<h1>{{nameAndSurname}}</h1>
<h1>Listado de series</h1>
<ol>
    <li v-for="(serie,index) in orderedMovies">{{serie}}</li>
</ol>
```

## Filtros

### 03x08_filtros

Si queremos modificar el resultado de cada elemento para mostrar el resultado de una determinada forma, tenemos que recurrir a los filtros. **vue.js** no proporciona ningún filtro dentro de su librería, como por ejemplo, poner en mayúsculas el resultado. Existe otra librería [vue2-filters](https://www.npmjs.com/package/vue2-filters) con muchos filtros ya preparados. Pero para aprender, vamos a crear los nuestros.

Vamos a crear una lista computada de búsqueda y le vamos a aplicar un filtro de mayúsculas utilizando el símbolo de tubería `|`. Los filtros se declaran en el _javascript_ al inicio como podremos ver:

>main.js

```js
Vue.filter('uppercase', (value) => value.toUpperCase());

new Vue({
    el: 'main',
    data: {
        fruits: [
            {name: 'Apple',season: 'winter',price: 'low'},
            {name: 'Orange',season: 'autumn',price: 'mid'},
            {name: 'Raspberry',season: 'spring',price: 'high'},
            {name: 'Watermelon',season: 'summer',price: 'mid'}
        ],
        searchParameter: null
    },
    computed: {
        searchFruit() {
            return this.fruits.filter((fruit) =>
                fruit.name.includes(this.searchParameter)
            );
        }
    }
});
```

>index.html

```html
<h4>Buscar fruta</h4>
<input type="search" v-model="searchParameter" placeholder="Buscar fruta">
<ol>
    <li v-for="(fruit,index) in searchFruit">
        {{fruit.name | uppercase}}
    </li>
</ol>
```

## Varias Instancias

### 03x09_varias_instancias

Por ahora hemos estado trabajando con un solo objeto `Vue`. Este objeto trabaja entre el **DOM** y la lógica de nuestro _javascript_. Hacer esto es muy sencillo, las instancias se crean con `#` dentro del elemento `el` y luego se pueden hacer referncia con un `id` dentro del _html_:

>main.js

```js
const vue2 = new Vue({
    el: '#app',
    data: {
        text: 'Second instance of Vue'
    }
});

const vue3 = new Vue({
    el: '#vue3',
    data: {
        text: 'Third instance of Vue'
    }
});
```

>index.html

```html
<body>
    <div id="app">
        {{text}}
    </div>

    <div id="vue3">
        {{text}}
    </div>
</body>
```

## Binding de clases

### 03x10_binding_clases

Si queremos cambiar atributos de la vista _html_ directamente desde **vue.js** existe el sistema de _binding_ para poder cambiar las propiedades según variables definidas en _javascript_.

Para el ejemplo queremos cambiar el estilo css de un elemento de una lista. Para eso vamos a crear un estilo css que se llama `like`:

> index.html

```html
<head>
    <style>
        .like{
            background: green;
            color: white;
            padding: 20px;
        }
    </style>
</head>
```

Este estilo lo vamos a aplicar a la lista de frutas cuando hagamos click en algun elemento de esa lista, pero solamente cambiará al estilo `like` el último elemento _clickado_. Por eso vamos a necesitar un nuevo método en el elemto de la lista `@click="checkFruit(index)"` y por fin el binding de clases `v-bind:class="{like: index == confirmed}"` que aplicará el estilo `like` cuando la variable `confirmed` tenga el mismo valor que el `index` del elemento a dibujar.

>main.js

```js
    data: {
            confirmed: null
        },
        methods: {
            checkFruit(id) {
                this.confirmed = id;
            }
        }
```

>index.html

```html
<ol>
    <li @click="checkFruit(index)" v-bind:class="{like: index == confirmed}" v-for="(fruit,index) in searchFruit">
        {{fruit.name | uppercase}}
    </li>
</ol>
```

## Ajax y HTTP

### 03x11_ajax_http

**vue.js** no incorpora ningún método de petiticiones AJAX para hacer llamadas a APIs externas. Por eso necesitamos usar una librería externa. Vamos a usar **AXIOS** - [https://github.com/axios/axios](https://github.com/axios/axios) para este fin, pero por ahora usaremos la librería que proviene del CDN: [https://unpkg.com/axios/dist/axios.min.js](https://unpkg.com/axios/dist/axios.min.js)

Para los ejemplos usaremos [*jsonplaceholder*](https://jsonplaceholder.typicode.com/) que contiene una API falsa para hacer pruebas de peticiones AJAX.

Necesitamos inicializar AXIOS en nuestra instancia de Vue, para eso llamaremos a `mounted(){},` que estará definido después de `el:`  y ejecutará lo que contenga nada más se cargue nuestra instancia de Vue.

>main.js

```js
mounted() {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then((result) => {
                this.posts = result.data;
            });
    },
    data: {
        posts: null
    }
```

>index.html

```html
<h1>Resultado del get:</h1>
<ul v-if="posts">
    <li v-for="post in posts">{{post.title}}</li>
</ul>
<span v-else>Cargando lista de posts</span>

</main>

<script src="https://unpkg.com/vue@2.4.4/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="js/main.js"></script>
</body>
```

# COMPONENTES

## Componentes básicos

### 04x01_componentes_basicos

Un componente es un bloque de la app, para que pueda ser reutilizados más adelante por diferentes partes de la app.

Al definir un componente `Vue.component('movies',{})`, podremos usarlo dentro de la _vista html_ como si fuera una etiqueta más `<movies></movies>`. El contenido estará definido dentro del parámetro `template():`` ` donde podremos poner el código _html_ a mostrar. Dentro de cada componente también se pueden añadir datos `data(){}` y funciones.

>main.js

```js
Vue.component('fruits', {
    template: `
        <h1>Component Fruits</h1>
    `
});

Vue.component('movies', {
    template: `
        <h1>Component {{title}}</h1>
    `,
    data() {
        return {
            title: 'MOVIES',
        };
    }
});

Vue.component('posts', {
    template: `
        <div class="component-posts">
            <ul v-if="posts">
                <li v-for="post in posts">{{post.title}}</li>
            </ul>
            <span v-else>Cargando lista de posts</span>
        </div>
    `,
    mounted() {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then((result) => {
                this.posts = result.data;
            });
    },
    data() {
        return {
            posts: null
        }
    }
})
```

>index.html

```html
<main>
    <fruits></fruits>
    <movies></movies>
    <h1>Get mediante component:</h1>
    <posts></posts>
</main>
```

## Plantillas en línea y props

### 04x02_plantillas_inline_props

Si queremos pasar valores desde nuestra instancia principal de **vue** a uno de nuestros componentes debemos usar `props:['fruit']`. Al definir la propiedad `fruit` podemos pasar valores a través de la vista _html_ dentro de la etiqueta de la plantilla `<fruits :fruit="superfruit"></fruits>`. **PERO** si queremos utilizar dentro de la etiqueta de la plantilla el valor por parámetro `props`, tenemos que incluir `inline-template`.

>main.js

```js
Vue.component('fruits', {
    props: ['fruit'],
    mounted() {
        if (this.fruit) {
            console.log(this.fruit);
        }
    }
});
```

>index.html

```html
<fruits :fruit="superfruit" inline-template>
    <div>
        <h1>Component Fruits</h1>
        <h3 v-if="fruit">{{fruit.name}}</h3>
    </div>
</fruits>
```

## Componentes dentro de componentes

### 04x03_componentes_dentro_de_componentes

Tal y como indica el título podemos anidar componentes facilmente:

>main.js

```js
Vue.component('master', {
    template: `
        <div>
            <h1>Master component</h1>
            <child></child>
        </div>
    `,
});

Vue.component('child', {
    template: `
        <div>
            <p style="background: yellow;">Soy un parrafo hijo</p>
        </div>
    `,
});
```

>index.html

```html
<master></master>
```

## Componentes dinámicos

### 04x05_componentes_dinamicos

Una opción interesante es poder cargar componentes dinámicamente dentro de la vista _html_. Para esto utilizaremos la misma lógica de _data binding_ pero aplicadas a una nueva etiqueta _html_ `<component>`. Le asignamos una propiedad `:is=""` y su parámetro será un valor de nuestra instancia **vue** que tendrá el nombre del componente que queramos cargar. 

En el ejemplo vamos a crear 3 botones que cambiarán el valor de esa variable y podrá cargar diferentes componentes dinámicamente:

>main.js

```js
Vue.component('posts', {
    template: `
        <div class="component-posts">
            <ul v-if="posts">
                <li v-for="post in posts">{{post.title}}</li>
            </ul>
            <span v-else>Cargando lista de posts</span>
        </div>
    `,
    mounted() {
        axios.get('https://jsonplaceholder.typicode.com/posts')
            .then((result) => {
                this.posts = result.data;
            });
    },
    data() {
        return {
            posts: null
        }
    }
})

Vue.component('master', {
    template: `
        <div>
            <h1>Master component</h1>
            <child></child>
        </div>
    `,
});

Vue.component('child', {
    template: `
        <div>
            <p style="background: yellow;">Soy un parrafo hijo</p>
        </div>
    `,
});

new Vue({
    el: 'main',
    data: {
        choosen: null
    }
});
```

>index.html

```html
<button @click="choosen = 'posts'">Cargar articulos</button>
<button @click="choosen = 'master'">Cargar master</button>
<button @click="choosen = 'child'">Cargar child</button>
<component :is="choosen"></component>
```

# Desarrollando una web SPA desde cero

## Vue cli

### 05x01_vue_cli

Instalar **vue-cli** con `npm`

`npm install -g vue-cli`

Esta herramienta tiene unos proyectos predeterminados que podremos cargar como proyecto base. Vamos a usar el **webpack-simple** que nos ayudará a tener una base sencilla con la que poder trabajar.

`vue init webpack-simple webapp`

A continucación nos hace una serie de preguntas, para poder configurar nuestro proyecto. Nos preguntan si queremos **SASS**, decimos que sí, es un procesador de CSS para poder manejar los estilos de manera más eficiente compilandolo.

Cuando termina de preparar el proyecto nos dice las siguientes instrucciones para compilar el proyecto:

```sh
cd webapp
npm install
npm run dev.
```

Al ejecutar `npm run dev` nos ejecutará el servidor local en [http://localhost:8080](http://localhost:8080) y poder ver allí el resultado.

Podemos comprobar que dentro de la estructura que nos crea **vue-cli** tenemos un nuevo fichero con extensión `.vue` que será un ejemplo de cómo vamos a crear componentes. Dentro de este nuevo fichero, encontramos 3 bloques estructurados en:

* `<template></template>` Será la parte de la vista _html_ del componente.

* `<script></script>` Toda la lógica estará definida en este bloque.

* `<style lang="scss"></style>` El estilo de la vista _html_ escrita en CSS **SASS**.

## Crear componentes en Vue CLI (VueJS 2)

### 05x02_crear_componentes_en_vue_cli

Como ya hemos visto antes, lo bueno de **vue** es crear componentes reutilizables. Para añadir nuevos componentes en nuestra nueva SPA, tenemos que crear un nuevo fichero `.vue` dentro de `/src` y hacer refencia desde `main.js`. Vamos a crear por ejemplo `Home.js` y vamos a utilizar este componente dentro de `App.vue`:

>Home.vue

```vue
<template>
    <h2>{{text}}</h2>
</template>

<script>
export default {
    name: 'home',
    data (){
        return {
            text: "Página HOME"
        }
    }
}
</script>
```

>App.vue

```vue
<template>
  <div id="app">
    <home></home>
  </div>
</template>
```

>main.js

```js
import Home from'./Home.vue'

Vue.component('home', Home);
```

## Crear múltiples componentes

### 05x03_crear_multiples_componentes

### 05x04_instalar_dependencias_proyecto

### 05x05_configurar_router

Al estar creando una SPA, los enlaces no cargan una nueva página, ya que está todo en una sola página, y estaremos llamando a diferentes componentes cada vez que queramos movernos por la Web App.

Necesitamos intalar el módulo `vue-router` para eso utilizaremos `npm` con la opción `--save` para que la dependencia se quede guardada dentro del fichero de dependencias `packages.json`

`npm install vue-router --save`

También vamos a aprovechar para instalar el módulo de **AXIOS** para realizar llamadas HTTP.

`npm install axios --save`

Ahora que tenemos las dependencias instaladas, vamos a importarlas en nuestro proyecto para usarlas.

Para usar **vue-router** tenemos que definir una constante en formato _json_ con el _path_ y el componente a cargar. Previamente tenemos que cargar en nuestra instancia de **Vue** el módulo de _vue-router_ con `Vue.use(VueRouter);`

También tenemos que instanciar un objeto `VueRouter()` que tiene como parámetro las rutas definidas en la constante `routes` y otro parámetro `mode: 'history'` que define la manera de navegar entre los componentes.

Una cosa más, debemos añadir el objeto que acabamos de crear `router` en los parámetros de las instancia de `Vue`.

>main.js

```js
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Home from'./Home.vue'
import Contact from'./Contact.vue'

Vue.use(VueRouter);

const routes = [
  {path: '/', component: Home},
  {path: '/home', component: Home},
  {path: '/contacto', component: Contact},
];

const router = new VueRouter({
  routes,
  mode: 'history'
});

Vue.component('home', Home);
Vue.component('contact', Contact);

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
```

Ya que tenemos definidas las rutas, debemos saber cómo utilizarlas. Para eso tenemos la nueva etiqueta `<router-view></router-view>` que cargará el componente dependiendo de la ruta que tenga el navegador. Por ejemplo, vamos a ponerlo dentro de nuestra `App.vue` y vamos a cambiar entre [http://localhost:8080](http://localhost:8080) y [http://localhost:8080/contacto](http://localhost:8080/contacto) para ver los diferentes componentes que cargará:

>App.vue

```vue
<template>
  <div id="app">
    <router-view></router-view>
  </div>
</template>
```

## Navegación y sistema de rutas

### 05x06_navegacion_sistema_rutas

Ahora que sabemos cómo cargar el contenido de una ruta, necesitamos saber cómo hacer que el usuario pueda cambiar entre rutas fácilmente. Para eso en vez de utilizar el clásico `<a></a>` debemos usar `<router-link></router-link>` con la propiedad `to=""`  para indicar al path que se va dirigir.

Existen momentos en los que queremos pasar parámetros a una determinada _url_ para ello debemos definirlo en nuestra constante de rutas `routes` del fichero `main.js` dentro del parámetro `path` podemos añadir con `:` identificadores que luego podremos sustituir. Estos parametros tendrán el nombre que le pongamos con `name:''` para luego sustituirlo.

`{path: '/path/:id', name:'objectId', component: vueComponent}`

La manera de sustiuir estos elementos, es dentro de la etiqueta `<router-link>` y ahora pasaremos un objeto json en su parámetro `:to={name:'objectId', params:{id: 16}}` para que pueda sustituir lo que aquí definamos.

Pero lo más interesante es recoger este parámetro de la ruta dentro del componente. Para eso utilizamos el método `mounted(){}` que se cargará nada más abrir ese componente y debemos recoger el valor con `this.$route.params.id`. El ejemplo completo quedaría así:

>main.js

```js
import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Home from'./Home.vue'
import Contact from'./Contact.vue'
import RestaurantList from'./RestaurantList.vue'
import RestaurantTop from'./RestaurantTop.vue'

Vue.use(VueRouter);

const routes = [
  {path: '/', component: Home},
  {path: '/home', component: Home},
  {path: '/contacto', component: Contact},
  {path: '/restaurantes', component: RestaurantList},
  {path: '/restaurantTop/:id', name:'restaurantTop', component: RestaurantTop},
];

const router = new VueRouter({
  routes,
  mode: 'history'
});

Vue.component('home', Home);
Vue.component('contact', Contact);
Vue.component('restaurant-list', RestaurantList);
Vue.component('restaurant-top', RestaurantTop);

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
```

>App.vue

```vue
<ul>
    <li>
        <router-link to="/home">Home</router-link>
    </li>
    <li>
        <router-link to="/restaurantes">Restaurantes</router-link>
    </li>
    <li>
        <router-link :to="{name:'restaurantTop', params:{id:16}}">Restaurantes destacados</router-link>
    </li>
    <li>
        <router-link to="/contacto">Contacto</router-link>
    </li>
</ul>
```

>RestaurantTop.vue

```vue
<template>
    <h2>{{text}} - ID = {{id_restaurant}}</h2>
</template>

<script>
export default {
    name: 'restaurant-top',
    mounted(){
        this.id_restaurant = this.$route.params.id;
    },
    data(){
        return {
            text: "Página Restaurante TOP",
            id_restaurant: null
        }
    }
}
</script>
```

## El servicio REST (api rest backend)

### 05x07_api_rest_backend

Ahora vamos a llamar a la API de Google Places para recibir una lista de restaurantes. Si estamos ejecutanto la app desde localhost deberemos tener cuidado con el **CORS** ya que algunas API no permiten cruce de dominios y debes instalar algún plugin para tu navegador (Firefox | Chrome).

Debemos instalar el módulo de AXIOS en nuestro proyecto con `npm`

`npm install axios --save`

Ahora solamente tenemos que importar axios en el componente que queramos usar:
`import axios from 'axios';`

>RestaurantList.vue

```vue
<template>
    <div>
        <ul id="restaurantList" v-if="restaurantList">
            <li v-for="restaurant in restaurantList" :key="restaurant.id">
                <strong>{{restaurant.name}}</strong>
            </li>
        </ul>
        <span v-else>Cargando restaurantes</span>
    </div>
</template>

<script>
import axios from 'axios';
export default {
    name: 'restaurant-list',
    mounted() {
        this.getRestaurantList();
    },
    data() {
        return {
            text: "Página Lista de restaurantes",
            restaurantList: null
        }
    },
    methods: {
        getRestaurantList() {
            axios.defaults.baseURL = 'https://maps.googleapis.com/maps/api/place/nearbysearch/';
            const params = '/json?location=40.46023464274291,-3.684593439102173&radius=500&type=restaurant&keyword=cruise&key= AIzaSyBDQWIOEMBnMuy_HRsdQeQp0UDPJPnTG70';

            axios.get(params)
                .then((response) => {
                    this.restaurantList = response.data.results;
                });
        }
    }
}
</script>
```

## Mostrar datos de los restaurantes y maquetar con CSS y SASS

### 05x09_maquetar_lista

Tan sencillo como utilizar la sección para estilos dentro del componente de **vue**. Utiliza un preprocesado **SASS** para CSS pero que no veremos ahora. Simplemente podremos poner unas propiedades y será solamente válidas para este componentes. Además vamos a incluir unos enlaces como hemos visto anteriormente:

>RestaurantList.vue

```vue
<template>
    <div>
        <ul id="restaurantList" v-if="restaurantList">
            <li v-for="restaurant in restaurantList" :key="restaurant.id">
                <strong>{{restaurant.name}}</strong>
                <p>
                    <router-link :to="{name:'restaurantDetail', params:{id: restaurant.id}}">ver</router-link>
                </p>
            </li>
        </ul>
        <span v-else>Cargando restaurantes</span>
    </div>
</template>

<style lang="scss">
    #restaurantList {
        padding: 5px;
        li {
            margin-top: 10px;
            width: 30%;
            height: 120px;
            border: 1px solid #dddddd;
            background: #eeeeee;
            padding: 20px;
            overflow: hidden;
        }
    }
</style>
```

>main.js

```js
const routes = [
  {path: '/', component: Home},
  .
  .
  .
  {path: '/restaurantDetail/:id', name:'restaurantDetail', component: RestaurantTop},
];
```

## Detalle restaurante

### 05x10_detalle_restaurante

