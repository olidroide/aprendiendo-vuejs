import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Home from'./Home.vue'
import Contact from'./Contact.vue'
import RestaurantList from'./RestaurantList.vue'
import RestaurantTop from'./RestaurantTop.vue'
import RestaurantDetail from'./RestaurantDetail.vue'

Vue.use(VueRouter);

const routes = [
  {path: '/', component: Home},
  {path: '/home', component: Home},
  {path: '/contacto', component: Contact},
  {path: '/restaurantes', component: RestaurantList},
  {path: '/restaurantTop/:id', name:'restaurantTop', component: RestaurantTop},
  {path: '/restaurantDetail/:id', name:'restaurantDetail', component: RestaurantDetail},
];

const router = new VueRouter({
  routes,
  mode: 'history'
});

Vue.component('home', Home);
Vue.component('contact', Contact);
Vue.component('restaurant-list', RestaurantList);
Vue.component('restaurant-top', RestaurantTop);
Vue.component('restaurant-detail', RestaurantDetail);

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
